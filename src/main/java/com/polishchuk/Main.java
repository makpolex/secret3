package com.polishchuk;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        secret(new Scanner(System.in));

    }

    public static String secret(Scanner scanner) {
        StringBuilder stringBuilder = new StringBuilder();

        System.out.print("Enter a: ");
        stringBuilder.append("Enter a: ");
        double a = scanner.nextDouble();
        stringBuilder.append(a).append('\n');

        System.out.print("Enter b: ");
        stringBuilder.append("Enter b: ");
        double b = scanner.nextDouble();
        stringBuilder.append(b).append('\n');

        System.out.print("Select an operation (+, -, /, *): ");
        stringBuilder.append("Select an operation (+, -, /, *): ");
        String operations = scanner.next();
        stringBuilder.append(operations).append("\n\n");
        System.out.println();

        String temp = "";
        for (int i = 0; i < operations.length(); i++) {

            switch (operations.charAt(i)) {

                case '+':
                    temp = a + " + " + b + " = " + (a + b);
                    System.out.println(temp);
                    stringBuilder.append(temp).append('\n');
                    break;

                case '-':
                    temp = a + " - " + b + " = " + (a - b);
                    System.out.println(temp);
                    stringBuilder.append(temp).append('\n');
                    break;

                case '/':
                    temp = a + " / " + b + " = " + (a / b);
                    System.out.println(temp);
                    stringBuilder.append(temp).append('\n');
                    break;

                case '*':
                    temp = a + " * " + b + " = " + (a * b);
                    System.out.println(temp);
                    stringBuilder.append(temp).append('\n');
                    break;
            }
        }

        return stringBuilder.toString();
    }
}