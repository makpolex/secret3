import com.polishchuk.Main;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class STest {

    private final String in_test = "src/test/resources/in_test",
            out_test = "src/test/resources/out_test";

    @Test
    public void test1() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(out_test));
            String line = "";
            StringBuilder answer_builder = new StringBuilder();

            while(line != null) {
                line = bufferedReader.readLine();

                if (line != null)
                    answer_builder.append(line).append('\n');
            }

            assert (Main.secret(new Scanner(new BufferedReader(new FileReader(in_test)))).equals(answer_builder.toString()));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
